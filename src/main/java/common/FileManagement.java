package common;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileManagement {

    private static String debtorDepositNumber = "1.2.3.4";
    private static String debtorAmount = "9990";
    private static int employeeNumber = 999;
    private String creditorAmount = String.valueOf(Integer.parseInt(debtorAmount) / employeeNumber);

    static Logger logger = Logger.getLogger(PaymentSystem.class);

    public Path createPaymentFile() {

        String debtor = "debtor\t" + debtorDepositNumber + "\t" + debtorAmount;

        String[] staffs = new String[employeeNumber];
        for (int i = 0; i < employeeNumber; i++) {

            String staffDepositNumber = String.format(debtorDepositNumber + ".%d ", i + 1);
            String staffAmount = creditorAmount;
            staffs[i] = "creditor\t" + staffDepositNumber + "\t" + staffAmount;
        }

        Path paymentPath = Paths.get("paymentDirectory/payment3.txt");
        try {
            if (!Files.exists(paymentPath)) {

                Files.createFile(paymentPath);
            } else {
                logger.info("payment file already exists.");
            }

            Files.write(paymentPath, debtor.getBytes(), StandardOpenOption.APPEND);
            Files.write(paymentPath, System.getProperty("line.separator").getBytes(), StandardOpenOption.APPEND);

            for (int i = 0; i < employeeNumber; i++) {
                Files.write(paymentPath, staffs[i].getBytes(), StandardOpenOption.APPEND);
                Files.write(paymentPath, System.getProperty("line.separator").getBytes(), StandardOpenOption.APPEND);
            }


        } catch (FileNotFoundException e) {
            logger.error("payment file not found");
        } catch (IOException e) {
            System.out.println(e.getClass() + "\t" + e.getMessage());
        }
        return paymentPath;
    }

    public Path createDepositFile() {
        Path depositPath = Paths.get("paymentDirectory/deposit.txt");
        try {
            if (!Files.exists(depositPath)) {

                depositPath = Files.createFile(depositPath);
            } else {
                logger.info("deposit file already exists.");
            }
            String bank = debtorDepositNumber + "\t" + debtorAmount;
            Files.write(depositPath, bank.getBytes(), StandardOpenOption.APPEND);
            Files.write(depositPath, System.getProperty("line.separator").getBytes(), StandardOpenOption.APPEND);

        } catch (FileNotFoundException e) {
            logger.error("deposit file not found");
        } catch (IOException e) {
            logger.error(e.getClass() + "\t" + e.getMessage());

        }
        return depositPath;
    }

    public Path createTransactionFile() {
        Path transactionPath = Paths.get("paymentDirectory/transaction.txt");

        try {
            if (!Files.exists(transactionPath)) {
                transactionPath = Files.createFile(transactionPath);
            } else {
                logger.info("transaction file already exists.");
            }
        } catch (FileNotFoundException e) {
            logger.error("transaction file not found");
        } catch (IOException e) {
            logger.error(e.getClass() + "\t" + e.getMessage());
        }
        return transactionPath;
    }

}
