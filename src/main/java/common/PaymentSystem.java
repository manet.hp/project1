package common;

import exceptionhadling.FialFileWritingException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


public class PaymentSystem {
    private String debtorDepositNumber;
    private int threadNumber;
    private int totalPaymentRecord;
    private int recordNumberPerThread = 100;
    List<Payment> paymentList = new ArrayList<>();
    ExecutorService executor = Executors.newFixedThreadPool(5);
    int debtAmount;
    AtomicInteger totalAmount = new AtomicInteger();


    private Path paymentPath;
    private Path depositPath;
    private Path transactionPath;

    static Logger logger = Logger.getLogger(PaymentSystem.class);
    FileManagement fileManagement = new FileManagement();


    public void paySalary()  {

        paymentPath = fileManagement.createPaymentFile();
        depositPath = fileManagement.createDepositFile();
        transactionPath = fileManagement.createTransactionFile();
        createPaymentList();

    }


    public void createPaymentList()  {
        List<String> paymentData;
        try {
            paymentData = Files.readAllLines(paymentPath);
        } catch (IOException e) {
            throw new FialFileWritingException("fail reading the payment value file.");
        }
        for (String payments : paymentData) {
            Payment payment = new Payment();
            String[] data = payments.split("\t");
            payment.setRole(data[0]);
            payment.setDepositNumber(data[1]);
            payment.setAmount(data[2]);
            if (!payment.getRole().equals("debtor")) {
                totalAmount.addAndGet(Integer.parseInt(payment.getAmount()));
            } else {
                debtAmount += (Integer.parseInt(payment.getAmount()));
                debtorDepositNumber = payment.getDepositNumber();
            }
            paymentList.add(payment);
        }
        totalPaymentRecord = paymentList.size();
        checkPayment();
    }

    public void checkPayment() {
        synchronized (Payment.class) {
            if (totalAmount.get() <= debtAmount) {
                doPayment();
            } else {
                logger.info("debtor amount is not enough for payment");
            }
        }
    }


    public void doPayment() {

        int fromIndex = 0;
        threadNumber = totalPaymentRecord / recordNumberPerThread;
        for (int i = 0; i < threadNumber; i++) {
            int toIndex = fromIndex + (recordNumberPerThread);
            List<Payment> paymentListPerThread = new ArrayList<>(paymentList.subList(fromIndex, toIndex));
            fromIndex += recordNumberPerThread;
            Runnable runnable = new PaymentOperation(depositPath, transactionPath, paymentListPerThread, totalAmount, debtorDepositNumber);
            executor.execute(runnable);

        }
        executor.shutdown();
    }


}

