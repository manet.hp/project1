package common;

import exceptionhadling.FialFileWritingException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PaymentOperation implements Runnable {
    private String threadName;
    AtomicInteger totalAmount;
    String debtorDepositNumber = "";
    String transactioData;
    List<Payment> paymentList;
    static Logger logger = Logger.getLogger(PaymentOperation.class);
    Path depositPath;
    Path transactionPath;


    public PaymentOperation(Path depositPath, Path transactionPath, List<Payment> paymentListPerThread, AtomicInteger totalAmount, String debtorDepositNumber) {
        this.depositPath = depositPath;
        this.transactionPath = transactionPath;
        this.paymentList = paymentListPerThread;
        this.totalAmount = totalAmount;
        this.debtorDepositNumber = debtorDepositNumber;
    }

    public void run() {
        threadName = Thread.currentThread().getName();
        synchronized (Payment.class) {
            for (int paymentIndex = 0; paymentIndex < paymentList.size(); paymentIndex++) {

                if (!paymentList.get(paymentIndex).getRole().equals("debtor")) {
                    logger.info(threadName);

                    String depositData = paymentList.get(paymentIndex).getDepositNumber() + "\t" + paymentList.get(paymentIndex).getAmount() + "\t";
                    transactioData = debtorDepositNumber + "\t" + depositData;

                    writeDepositFile(depositPath, depositData);

                    writeTransactionFile(transactionPath, transactioData);

                } else if (paymentList.get(paymentIndex).getRole().equals("debtor")) {
                    logger.info(threadName);
                    int temp = Integer.parseInt(paymentList.get(paymentIndex).getAmount()) - totalAmount.get();
                    paymentList.get(paymentIndex).setAmount(String.valueOf(temp));
                    String depositData = paymentList.get(paymentIndex).getDepositNumber() + "\t" + paymentList.get(paymentIndex).getAmount() + "\t";

                    writeDepositFile(depositPath, depositData);

                }
            }
        }
    }


    public void writeDepositFile(Path depositPath, String depositData) {
        synchronized (Payment.class) {
            try {
                Files.write(depositPath, depositData.getBytes(), StandardOpenOption.APPEND);
                Files.write(depositPath, System.getProperty("line.separator").getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                throw new FialFileWritingException("fail writing into transaction file.");
            }
        }
    }

    public void writeTransactionFile(Path transactionPath, String transactioData) {
        synchronized (Payment.class) {
            try {
                Files.write(transactionPath, transactioData.getBytes(), StandardOpenOption.APPEND);
                Files.write(transactionPath, System.getProperty("line.separator").getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                throw new FialFileWritingException("fail writing into deposit file.");
            }

        }
    }
}